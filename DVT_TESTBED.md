# testbed for dvt failover


## variables
- server = 7keepalived-01 (172.27.0.51) + 7keepalived-02 (172.27.0.52)


## server
#### dummy interface enp2s0:
```
modprobe dummy
ip link set name enp2s0 dev dummy0
ifconfig enp2s0 up
```

#### rename eth0 to enp3s0:
```
( ifconfig eth0 down ; ip link set eth0 name enp3s0 ; ifconfig enp3s0 up ) &
```


#### files
- [/etc/keepalived/notify.sh](notify.sh)
- [/etc/keepalived/tun4dvt2lv.sh](tun4dvt2lv.sh)
- [/etc/conntrackd/primary-backup.sh](primary-backup.sh)
- [/etc/sysctl.d/keepalived.conf](sysctl_keepalived.conf)


## 7keepalived-01 (dvtfw)
#### turn 7keepalived-01 to dvtfw:
```
hostname dvtfw
ip addr add 172.24.0.254/16 dev enp3s0
```

#### files
- [/etc/keepalived/keepalived.conf](keepalived-dvt.conf)
- [/etc/conntrackd/conntrackd.conf](conntrackd-dvt.conf)


## 7keepalived-02 (dvtauxfw)
#### turn 7keepalived-02 to dvtauxfw:
```
hostname dvtauxfw
ip addr add 172.24.0.253/16 dev enp3s0
```

#### files
- [/etc/keepalived/keepalived.conf](keepalived-dvtaux.conf)
- [/etc/conntrackd/conntrackd.conf](conntrackd-dvtaux.conf)


## 7keepalived-03 
#### turn 7keepalived-03 to remote tunnel endpoint:
```
ip addr add 172.25.0.1/28 dev eth0
ip tunnel add tun4dvt2lv mode ipip remote 172.25.0.10 local 172.25.0.1
ip addr add 10.0.4.5/29 dev tun4dvt2lv
```
