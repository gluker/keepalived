#!/bin/bash
# Copyright (c) 2015 AG

LOCAL_DC="DVT"
LOCAL_EXT_IP="172.25.0.10"
LOCAL_INT_IP="10.0.4.4"
LOCAL_INT_MASK="29"

REMOTE_DC="LV"
REMOTE_EXT_IP="172.25.0.1"
REMOTE_NET=( "172.25.0.0/16" "172.21.0.0/16" "172.22.0.0/16" "172.23.0.0/16" "172.26.0.0/16" )
REMOTE_INT_IP="10.0.4.5"

TUN_NAME=`echo tun4${LOCAL_DC}2${REMOTE_DC} | tr -s [A-Z] [a-z]`

description="Brings the tunnel between $LOCAL_DC & $REMOTE_DC"


start()
{
	echo "Up tunnel to $REMOTE_DC"

	#load tunnel module
	modprobe ipip

	#bring tunnel up
	ip tunnel add $TUN_NAME mode ipip remote $REMOTE_EXT_IP local $LOCAL_EXT_IP ttl 255

	#set local tunnel internal ip
	ifconfig $TUN_NAME $LOCAL_INT_IP/$LOCAL_INT_MASK up

	#set routes to remote networks
	for net in "${REMOTE_NET[@]}"
	do
	    route add -net $net dev $TUN_NAME
	done

	#test reach remote tunnel internal ip
	if ( ! ping -c 1 -w 3 -W 3 $REMOTE_INT_IP &> /dev/null ) ; then
            echo "Failed to up tunnel to $REMOTE_DC"
            exit 1
        fi
        exit 0
}


stop()
{
	echo "Down tunnel to $REMOTE_DC"

	#delete routes to remote networks
	for net in "${REMOTE_NET[@]}"
	do
	    route del -net $net dev $TUN_NAME
	done

	#bring tunnel down
	ip tunnel del $TUN_NAME

	#check innerface down
	if [ `ifconfig | grep -c $TUN_NAME` == 0 ] ; then
	    echo "Failed to down tunnel to $REMOTE_DC"
            exit 1
        fi
        exit 0
}


case $1 in
    "start")
        start
        ;;
    "stop")
        stop
        ;;
    *)        
        echo $description
        echo "run: $0 [start|stop]"
        exit 1
        ;;
esac
