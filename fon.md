# variables
- client = 7keepalived-03 (172.27.0.53)
- server = 7keepalived-01 (172.27.0.51) + 7keepalived-02 (172.27.0.52)
- all = client + server


# all

## commands
```
apt-get update
```


# client

## commands
```
route del default gw 172.27.0.1
route add default gw 172.27.0.54
```


# 7keepalived-01

## files
- [/etc/keepalived/keepalived.conf](keepalived-7keepalived-01.conf)
- [/etc/conntrackd/conntrackd.conf](conntrackd-7keepalived-01.conf)


# 7keepalived-02

## files
- [/etc/keepalived/keepalived.conf](keepalived-7keepalived-02.conf)
- [/etc/conntrackd/conntrackd.conf](conntrackd-7keepalived-02.conf)


# server

## files
- [/etc/conntrackd/primary-backup.sh](primary-backup.sh)

## commands
```
# iptables
iptables -t nat -A POSTROUTING -s 172.27.0.0/24 -j SNAT --to 172.27.0.54
iptables-save > /etc/iptables/rules.v4 

# sysctl
cat > /etc/sysctl.d/local.conf << EOF
net.ipv4.ip_forward = 1
net.netfilter.nf_conntrack_tcp_be_liberal = 1
net.netfilter.nf_conntrack_tcp_loose = 0
EOF
sysctl -p /etc/sysctl.d/local.conf

# saving to rc (sysctls like *nf_conntrack* not available before iptables)
echo -e "sysctl -p /etc/sysctl.d/local.conf\nexit 0" > /etc/rc.local

# apt
apt-get install keepalived conntrackd

# keepalived: config installed
/etc/init.d/keepalived restart
update-rc.d keepalived enable

# conntrackd: config installed
/etc/init.d/conntrackd restart
update-rc.d conntrackd enable
```

## extra installed with keepalived & conntrackd
```
The following extra packages will be installed:
  iproute ipvsadm libnetfilter-conntrack3 libnetfilter-cthelper0
  libnetfilter-queue1 libnl-3-200 libnl-genl-3-200 libperl5.20 libsensors4
  libsnmp-base libsnmp30 perl perl-base perl-modules
Suggested packages:
  heartbeat ldirectord lm-sensors snmp-mibs-downloader perl-doc
  libterm-readline-gnu-perl libterm-readline-perl-perl libb-lint-perl
  libcpanplus-dist-build-perl libcpanplus-perl libfile-checktree-perl
  libobject-accessor-perl
The following NEW packages will be installed:
  conntrackd iproute ipvsadm keepalived libnetfilter-conntrack3
  libnetfilter-cthelper0 libnetfilter-queue1 libnl-3-200 libnl-genl-3-200
  libperl5.20 libsensors4 libsnmp-base libsnmp30
The following packages will be upgraded:
  perl perl-base perl-modules
3 upgraded, 13 newly installed, 0 to remove and 19 not upgraded.
Need to get 10.6 MB of archives.
After this operation, 8,468 kB of additional disk space will be used.
```
