#!/bin/bash

TYPE=$1
NAME=$2
STATE=$3

case $STATE in
    "MASTER")
        /etc/keepalived/tun4dvt2lv.sh start
        /etc/conntrackd/primary-backup.sh primary
        exit 0
        ;;
    "BACKUP")
        /etc/keepalived/tun4dvt2lv.sh stop
        /etc/conntrackd/primary-backup.sh backup
        exit 0
        ;;
    "FAULT") 
        /etc/keepalived/tun4dvt2lv.sh stop
        /etc/conntrackd/primary-backup.sh fault
        exit 0
        ;;
    *)        
        echo "unknown state"
        exit 1
        ;;
esac
