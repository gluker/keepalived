# Резервное переключение канала в случае сбоя основного шлюза


## HSRP vs VRRP
Для увеличения доступности маршрутизаторов в случае выхода из строя одного из
них используется технология объединения группы маршрутизаторов в один 
виртуальный, которому назначается общий IP-адрес в качестве шлюза по умолчанию 
для компьютеров в сети. Технология реализуется на базе протоколов HSRP или VRRP, 
оба поддерживаются основными вендорами hardware маршрутизаторов (Cisco/Juniper),
однако протокол HSRP не поддерживается Linux/FreeBSD из-за своей закрытости.

Из-за этого для реализации резервного переключения был выбран VRRP, который к 
тому же является дальнейшей эволюцией протокола HSRP.

| [HSRP](https://ru.wikipedia.org/wiki/HSRP)                                                                          | [VRRP](https://ru.wikipedia.org/wiki/VRRP)                                                                                           |
|---------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------|
| Propietary                                                                                                          | Standards based                                                                                                                      |
| RFC 2281                                                                                                            | RFC 3768                                                                                                                             |
| Separate IP Address needed for the Virtual                                                                          | Can use the physical IP Address of the Virtual, if needed, saving IP space.                                                          |
| One Master, One Standby, all others are listening                                                                   | One Master, all other routers are backup                                                                                             |
| More familiar to most network engineers                                                                             | Less familiar - yet very similar                                                                                                     |
| Can track an interface for failover                                                                                 | Can track an interface for failover (depending on operating system and version, it can also track the reachability of an IP address) |
| All HSRP routers use multicast hello packets to 224.0.0.2 (all routers) for version 1 or 224.0.0.102 for version 2. | All VRRP routers use IP protocol number 112 (vrrp) to communicate via multicast IP address 224.0.0.18                                |
| All virtual router must use MAC address 0000.0c07.acXX where XX is the group ID.                                    | All virtual routers must use 00-00-5E-00-01-XX as its Media Access Control (MAC) address                                             |

(c) [Comparing HSRP Versus VRRP – Same Thing Only Different?](http://www.routerfreak.com/comparing-hsrp-versus-vrrp-same-thing-only-different/)


## vrrpd vs keepalived
В Linux протокол VRRP поддерживает ПО vrrpd и keepalived. Поиск гуглом по 
vrrpd: 12800 результатов, keepalived: 510000. 
В результате выбран keepalived, как наиболее популярное ПО.


## Настройка keepalived

Привести настройки sysctl в соответсвии со значениями:
```
sysctl -w net.ipv4.igmp_max_memberships=20
```

Пример конфигурации [keepalived.conf](keepalived.conf):
```
vrrp_sync_group VG_1 {
    group {
        lan_net
    }
    notify_master "/etc/conntrackd/primary-backup.sh primary"
    notify_backup "/etc/conntrackd/primary-backup.sh backup"
    notify_fault "/etc/conntrackd/primary-backup.sh fault"
}

vrrp_instance lan_net {
    state MASTER
    interface enp0s8
    virtual_router_id 10
    priority 101
    advert_int 1

    track_interface {
        enp0s9
    }

    virtual_ipaddress {
        10.0.2.200
    }

    authentication {
        auth_type PASS
        auth_pass koolpw
    }
}
```


## conntrackd: миграция состояний сессий для NAT

В случае использования NAT необходимо синхронизировать таблицу состояний сессий
между маршрутизаторами для сохранения работоспособности установленных соединений:
для этого необходимо установить и настроить conntrackd.

Привести параметры sysctl в соответствии со значениями:
```
sysctl -w net.netfilter.nf_conntrack_tcp_be_liberal=1
sysctl -w net.netfilter.nf_conntrack_tcp_loose=0
```

Пример конфигурации [conntrackd.conf](conntrackd.conf):
```
Sync {
        #Mode FTFW {
        #        DisableExternalCache On
        #}
	Mode ALARM {
		RefreshTime 15
		CacheTimeout 180
	}
        Multicast {
		IPv4_address 225.0.0.50
		Group 3780
		IPv4_interface 10.0.2.1
		Interface br-lan
		SndSocketBuffer 1249280
		RcvSocketBuffer 1249280
		Checksum on
	}
}
General {
	Nice -20
	HashSize 32768
	HashLimit 131072
	LogFile on
	LockFile /var/lock/conntrack.lock
	UNIX {
		Path /var/run/conntrackd.ctl
		Backlog 20
	}
	NetlinkBufferSize 2097152
	NetlinkBufferSizeMaxGrowth 8388608
	Filter From Userspace {
		Protocol Accept {
			TCP
			UDP
			ICMP
		}
		Address Ignore {
		    IPv4_address 127.0.0.1 
            IPv4_address 192.168.56.3
            IPv4_address 172.20.10.14
            IPv4_address 10.0.2.1
            IPv4_address 10.0.2.100
            IPv4_address 10.0.2.200
		}
	}
}
```


## [primary-backup.sh](primary-backup.sh)

```
#!/bin/sh
#
# (C) 2006-2011 by Pablo Neira Ayuso <pablo@netfilter.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Description:
#
# This is the script for primary-backup setups for keepalived
# (http://www.keepalived.org). You may adapt it to make it work with other
# high-availability managers.
#
# Do not forget to include the required modifications to your keepalived.conf
# file to invoke this script during keepalived's state transitions.
#
# Contributions to improve this script are welcome :).
#

CONNTRACKD_BIN=/usr/sbin/conntrackd
CONNTRACKD_LOCK=/var/lock/conntrack.lock
CONNTRACKD_CONFIG=/etc/conntrackd/conntrackd.conf

case "$1" in
  primary)
    #
    # commit the external cache into the kernel table
    #
    $CONNTRACKD_BIN -C $CONNTRACKD_CONFIG -c
    if [ $? -eq 1 ]
    then
        logger "ERROR: failed to invoke conntrackd -c"
    fi

    #
    # flush the internal and the external caches
    #
    $CONNTRACKD_BIN -C $CONNTRACKD_CONFIG -f
    if [ $? -eq 1 ]
    then
    	logger "ERROR: failed to invoke conntrackd -f"
    fi

    #
    # resynchronize my internal cache to the kernel table
    #
    $CONNTRACKD_BIN -C $CONNTRACKD_CONFIG -R
    if [ $? -eq 1 ]
    then
    	logger "ERROR: failed to invoke conntrackd -R"
    fi

    #
    # send a bulk update to backups 
    #
    $CONNTRACKD_BIN -C $CONNTRACKD_CONFIG -B
    if [ $? -eq 1 ]
    then
        logger "ERROR: failed to invoke conntrackd -B"
    fi
    ;;
  backup)
    #
    # is conntrackd running? request some statistics to check it
    #
    $CONNTRACKD_BIN -C $CONNTRACKD_CONFIG -s
    if [ $? -eq 1 ]
    then
        #
	# something's wrong, do we have a lock file?
	#
    	if [ -f $CONNTRACKD_LOCK ]
	then
	    logger "WARNING: conntrackd was not cleanly stopped."
	    logger "If you suspect that it has crashed:"
	    logger "1) Enable coredumps"
	    logger "2) Try to reproduce the problem"
	    logger "3) Post the coredump to netfilter-devel@vger.kernel.org"
	    rm -f $CONNTRACKD_LOCK
	fi
	$CONNTRACKD_BIN -C $CONNTRACKD_CONFIG -d
	if [ $? -eq 1 ]
	then
	    logger "ERROR: cannot launch conntrackd"
	    exit 1
	fi
    fi
    #
    # shorten kernel conntrack timers to remove the zombie entries.
    #
    $CONNTRACKD_BIN -C $CONNTRACKD_CONFIG -t
    if [ $? -eq 1 ]
    then
    	logger "ERROR: failed to invoke conntrackd -t"
    fi

    #
    # request resynchronization with master firewall replica (if any)
    # Note: this does nothing in the alarm approach.
    #
    $CONNTRACKD_BIN -C $CONNTRACKD_CONFIG -n
    if [ $? -eq 1 ]
    then
    	logger "ERROR: failed to invoke conntrackd -n"
    fi
    ;;
  fault)
    #
    # shorten kernel conntrack timers to remove the zombie entries.
    #
    $CONNTRACKD_BIN -C $CONNTRACKD_CONFIG -t
    if [ $? -eq 1 ]
    then
    	logger "ERROR: failed to invoke conntrackd -t"
    fi
    ;;
  *)
    logger "ERROR: unknown state transition"
    echo "Usage: primary-backup.sh {primary|backup|fault}"
    exit 1
    ;;
esac

exit 0
```